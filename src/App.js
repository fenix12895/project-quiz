import "./App.css";
import { useEffect, useState } from "react";
import getQuizes from "./api/getQuizes";
import Question from "./components/Question";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";
import Footer from "./components/Footer";
import Log from "./components/Log";
import QuizNavi from "./components/QuizNavi";


function App() {
  const [quizes, setQuizes] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [userAnswers, setUserAnswers] = useState([]);
  
  const handleCorrectAnswers = (answer, quizId) => {
      setUserAnswers([...userAnswers, { ...answer, quizId }])
  }

  const handleIsNext = () => {
    setTimeout(() => {
      setCurrentQuestion(currentQuestion + 1);
    }, 1000)
   
  };
  const handleIsCorrect = (id) => {
   
  };
  useEffect(() => {
    getQuizes().then((quizes) => {
      setQuizes(quizes);
      setIsLoading(false);
    });
  }, []);


  return (
    <div className="wrapper">
      <Header />
      <Sidebar />
      {!isLoading && quizes.length > currentQuestion && (
        <div>
          <QuizNavi quizes={quizes} userAnswers={userAnswers} quizId={quizes[currentQuestion].id}/>
          <Question
            currentQuestion={currentQuestion}
            correctAnswers={handleCorrectAnswers}
            quizesLength={quizes.length}
            quiz={quizes[currentQuestion]}
            isNext={handleIsNext}
            onClick={handleIsCorrect}
            
          />
        </div>
      )}
      {quizes.length <= currentQuestion && <Log userAnswers={userAnswers}/> }
      <Footer />
    </div>
  );
}

export default App;
